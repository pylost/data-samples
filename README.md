# data samples

	Test datasets for PyLOSt.
	A basic PyLOSt workflow is also available.
		Data can be loaded using the reload button in each Simple Loader widget.
	

## LTP_slp_format
	
	contains 3 full scans in slp format


## Metropro_dat_format

	contains a single stitching dataset without motors position (step x = +2.052mm)
	
	
## Veeco_MSI_opd_format

	contains a single stitching dataset containing the X/Y motors position of each subapertures


## data-samples.ows

	PyLOSt - Orange3 workflow
	
![Alt text](data-samples.png)
